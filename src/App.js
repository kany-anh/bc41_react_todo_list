import "./App.css";
import ToDoList from "./JSS_Styled_Components/ToDoList/ToDoList";

function App() {
  return (
    <div>
      <ToDoList />
    </div>
  );
}

export default App;
