import React, { Component } from "react";
import { createGlobalStyle, ThemeProvider } from "styled-components";
import { Container } from "../Components/Container";
import { Dropdown } from "../Components/Dropdown";
import { ToDoListDarkTheme } from "../Themes/ToDoListDarkTheme";
import { ToDoListLightTheme } from "../Themes/ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "../Themes/ToDoListPrimaryTheme ";
import {
  Heading1,
  Heading2,
  Heading3,
  Heading4,
  Heading5,
} from "../Components/Heading";
import { TextField } from "../Components/TextField";
import { Button } from "../Components/Button";
import { Table, Tr, Td, Th, Thead, Tbody } from "../Components/Table";
import { connect } from "react-redux";
import {
  addTaskAction,
  deleteTask,
  doneTask,
  editTask,
  selectTheme,
  updateTask,
} from "../redux/action/types/ToDoListAction";
import { arrTheme } from "../Themes/ThemeManager";

class ToDoList extends Component {
  state = {
    taskName: "",
    disable: true,
  };
  renderTaskToDo = () => {
    return this.props.taskList
      .filter((tasklist) => !tasklist.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.setState(
                    {
                      disable: false,
                    },
                    () => {
                      this.props.dispatch(editTask(task));
                    }
                  );
                }}
                className="fa fa-edit"
              ></Button>
              <Button
                onClick={() => {
                  this.props.dispatch(doneTask(task.id));
                }}
                className="fa fa-check"
              ></Button>
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTask(task.id));
                }}
                className="fa fa-trash"
              ></Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTaskCompleted = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTask(task.id));
                }}
                className="fa fa-trash"
              ></Button>
            </Th>
          </Tr>
        );
      });
  };
  handleChange = (e) => {
    let name = [...this.state.taskName];
    name = e.target.value;
    this.setState({ taskName: name });
  };

  renderTheme = () => {
    return arrTheme.map((theme, index) => {
      return (
        <option key={index} value={theme.id}>
          {theme.name}
        </option>
      );
    });
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps.taskEdit.id !== this.props.taskEdit.id) {
      this.setState({
        taskName: this.props.taskEdit.taskName,
      });
    }
  };

  render() {
    return (
      <ThemeProvider theme={this.props.themeToDoList}>
        <Container className="w-50">
          <Dropdown
            onChange={(e) => {
              let { value } = e.target;
              this.props.dispatch(selectTheme(value));
            }}
          >
            {this.renderTheme()}
          </Dropdown>
          <Heading3>To do list</Heading3>
          <TextField
            value={this.state.taskName}
            onChange={this.handleChange}
            name="taskName"
            label="Task name"
            className="w-50"
          ></TextField>
          <Button
            onClick={() => {
              let { taskName } = this.state;
              let newTask = {
                id: Date.now(),
                taskName: taskName,
                done: false,
              };
              this.props.dispatch(addTaskAction(newTask));
            }}
            className="ml-2"
          >
            <i className="fa fa-plus"></i> Add task
          </Button>
          {this.state.disable ? (
            <Button
              disabled={true}
              onClick={() => {
                console.log("hello");
                this.props.dispatch(updateTask(this.state.taskName));
              }}
              className="ml-2"
            >
              <i className="fa fa-upload"></i> Update task
            </Button>
          ) : (
            <Button
              onClick={() => {
                let { taskName } = this.state;
                this.setState(
                  {
                    disable: true,
                    taskName: "",
                  },
                  () => {
                    this.props.dispatch(updateTask(taskName));
                  }
                );
              }}
              className="ml-2"
            >
              <i className="fa fa-upload"></i> Update task
            </Button>
          )}

          <hr />
          <Heading3>Task to do</Heading3>
          <Table>
            <Thead>{this.renderTaskToDo()}</Thead>
          </Table>
          <Heading3>Task completed</Heading3>
          <Table>
            <Thead>{this.renderTaskCompleted()}</Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    themeToDoList: state.toDoListReducer.themeToDoList,
    taskList: state.toDoListReducer.taskList,
    taskEdit: state.toDoListReducer.taskEdit,
  };
};
export default connect(mapStateToProps)(ToDoList);
