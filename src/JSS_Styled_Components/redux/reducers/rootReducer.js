import { combineReducers } from "redux";
import { toDoListReducer } from "./toDoListReducer";

export let rootReducer = combineReducers({ toDoListReducer });
