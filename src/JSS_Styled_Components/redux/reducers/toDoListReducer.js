import { arrTheme } from "../../Themes/ThemeManager";
import { ToDoListDarkTheme } from "../../Themes/ToDoListDarkTheme";
import {
  ADD_TASK_TO_DO,
  DELETE_TASK,
  DONE_TASK,
  EDIT_TASK,
  SELECT_THEME,
  UPDATE_TASK,
} from "../contant/toDoListContant";

const initialState = {
  themeToDoList: ToDoListDarkTheme,
  taskList: [
    { id: 1, taskName: "task 1", done: true },
    { id: 2, taskName: "task 2", done: false },
    { id: 3, taskName: "task 3", done: true },
    { id: 4, taskName: "task 4", done: false },
  ],
  taskEdit: { id: -1, taskName: "", done: false },
};

export const toDoListReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TASK_TO_DO: {
      let newTaskList = [...state.taskList];
      let index = newTaskList.findIndex((task) => {
        return task.taskName === payload.taskName;
      });
      if (index !== -1) {
        alert("already exits");
        return { ...state };
      }
      newTaskList.push(payload);
      return { ...state, taskList: newTaskList };
    }
    case SELECT_THEME: {
      let THEME = arrTheme.find((item) => {
        return item.id === payload * 1;
      });
      if (THEME) {
        state.themeToDoList = THEME.theme;
      }
      return { ...state };
    }
    case DONE_TASK: {
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((task) => {
        return task.id === payload;
      });
      if (index !== -1) {
        taskListUpdate[index].done = true;
      }
      return { ...state, taskList: taskListUpdate };
    }
    case DELETE_TASK: {
      let taskListUpdate = [...state.taskList];
      taskListUpdate = taskListUpdate.filter((task) => {
        return task.id !== payload;
      });
      return { ...state, taskList: taskListUpdate };
    }
    case EDIT_TASK: {
      return { ...state, taskEdit: payload };
    }
    case UPDATE_TASK: {
      state.taskEdit = { ...state.taskEdit, taskName: payload };
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((task) => {
        return task.id === state.taskEdit.id;
      });
      if (index !== -1) {
        taskListUpdate[index] = state.taskEdit;
      }

      return {
        ...state,
        taskList: taskListUpdate,
        taskEdit: { id: -1, taskName: "", done: false },
      };
    }

    default:
      return state;
  }
};
